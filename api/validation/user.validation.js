import Joi from 'joi';
const minEmail = 3;
const maxEmail = 320;
const maxPass = 16;
const minPass = 8;

export const user = {
    schemaUser: Joi.object({
        name: Joi.string().required().empty().messages({
            'string.base': `name should be a type of 'text'`,
            'string.empty': `name cannot be an empty field`,
            'any.required': `name is a required field`}
        ),
        email_id: Joi.string().min(minEmail).max(maxEmail).required().email().empty().messages({
            'string.base': `email_id should be a type of 'text'`,
            'string.empty': `email_id cannot be an empty field`,
            'string.email': `email_id format not valid`,
            'any.required': `email_id is a required field`}
        ),
        password: Joi.string().min(minPass).max(maxPass).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).empty().messages({
            'string.base': `password should be a type of 'text'`,
            'string.empty': `password cannot be an empty field`,
            'string.min': 'password should be of minimum 8 characters',
            'string.max': 'password should be of maximum 255 characters',
            'any.required': `password is a required field`}
        ),
        contact_number: Joi.string().required().regex(/^\d{10}$/).empty().messages({
            'string.base': `contact_number should be a type of 'number'`,
            'string.empty': `contact_number cannot be an empty field`,
            'any.required': `contact_number is a required field`}
        ),
        username: Joi.string().required().empty().messages({
            'string.base': `username should be a type of 'text'`,
            'string.empty': `username cannot be an empty field`,
            'any.required': `username is a required field`}
        )}
)};

export const userUpdate = {
    schemaUser: Joi.object({
        name: Joi.string().optional().empty().messages({
            'string.base': `name should be a type of 'text'`,
            'string.empty': `name cannot be an empty field`,
            'any.optional': `name is a optional field`}
        ),
        email_id: Joi.string().min(minEmail).max(maxEmail).optional().email().empty().messages({
            'string.base': `email_id should be a type of 'text'`,
            'string.empty': `email_id cannot be an empty field`,
            'string.email': `email_id format not valid`,
            'any.optional': `email_id is a optional field`}
        ),
        password: Joi.string().min(minPass).max(maxPass).optional().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).empty().messages({
            'string.base': `password should be a type of 'text'`,
            'string.empty': `password cannot be an empty field`,
            'string.min': 'password should be of minimum 8 characters',
            'string.max': 'password should be of maximum 255 characters',
            'any.optional': `password is a optional field`}
        ),
        contact_number: Joi.string().optional().regex(/^\d{10}$/).empty().messages({
            'string.base': `contact_number should be a type of 'number'`,
            'string.empty': `contact_number cannot be an empty field`,
            'any.optional': `contact_number is a optional field`}
        ),
        username: Joi.string().optional().empty().messages({
            'string.base': `username should be a type of 'text'`,
            'string.empty': `username cannot be an empty field`,
            'any.optional': `username is a optional field`}
        )}
)};
