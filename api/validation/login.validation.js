import Joi from 'joi';
const minEmail = 3;
const maxEmail = 320;
const maxPass = 16;
const minPass = 8;

export const login = {
    schemaLogin: Joi.object({
        
        email_id_or_username: [
            Joi.string().required().empty().email().messages({
                'string.base': `email_id_or_username should be a type of 'text'`,
                'string.empty': `email_id_or_username cannot be an empty field`,
                'string.email': `email_id_or_username format not valid`,
                'any.required': `email_id_or_username is a required field`}),
            
            Joi.string().required().empty().messages({
                'string.base': `email_id_or_username should be a type of 'text'`,
                'string.empty': `email_id_or_username cannot be an empty field`,
                'any.required': `email_id_or_username is a required field`}),
        ],
        password: Joi.string().min(minPass).max(maxPass).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).empty().messages({
            'string.base': `password should be a type of 'text'`,
            'string.empty': `password cannot be an empty field`,
            'string.min': 'password should be of minimum 8 characters',
            'string.max': 'password should be of maximum 255 characters',
            'any.required': `password is a required field`}
        )}
    )};
