
export const loginModel = function login_schema( sequelize, Sequelize ){
    const Login = sequelize.define('login', {
        email_id_or_username: {
            type: Sequelize.STRING,
            minlength: 3,
            maxlength: 320,
            allowNull: false,
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
            minlength: 8,
            maxlength: 255
        }  
      }); 
    return Login;
}

