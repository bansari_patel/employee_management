
export const userModel = function user_schema( sequelize, Sequelize ){
    const User = sequelize.define('user', {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            trim: true,
            maxlength: 20
        },
        email_id: {
            type: Sequelize.STRING,
            minlength: 3,
            maxlength: 320,
            allowNull: false,
            unique: true
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
            minlength: 8,
            maxlength: 255
        },
        contact_number: {
            type: Sequelize.STRING,
            allowNull: true,
            minlength: 10,
            maxlength: 15
        },
        username:  {
            type: Sequelize.STRING,
            allowNull: true, 
            minlength: 5,
            maxlength: 15,
            unique: true
        }
    }); 
    return User;
}
