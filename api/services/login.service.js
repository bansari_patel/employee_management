import db from '../database/database.config.js';

const login = db.login;
export const login_create = async (loginData,callback) => {
    await login.create(loginData)
        .then(data => {
            callback(null, data);
    }).catch(err => {
        callback(err);
    });
    
};

