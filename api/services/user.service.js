import db from '../database/database.config.js';

const users = db.user
export const user_create = async (userData,callback) => {
    await users.create(userData)
        .then(data => {
            callback(null, data);
    }).catch(err => {
        callback(err);
    });
    
};

export const isUserExistById = async (id,callback) => {
    await users.findByPk(id)
        .then(UserData => {
            if(UserData == null){
                callback(null, null);
            }else{
                callback(null, UserData);
            }
        }).catch(err => {
            callback(err);
        });
};


export const isUserExistByEmail = async (email_id,callback) => {
    await users.findOne({where: {email_id}})
        .then(UserData => {
            if(UserData == null){
                callback(null, null);
            }else{
                callback(null, UserData);
            }
        }).catch(err => {
            callback(err);
        });
};

export const isUserExistByUsername = async (username,callback) => {
    await users.findOne({where: {username}})
        .then(UserData => {
            if(UserData == null){
                callback(null, null);
            }else{
                callback(null, UserData);
            }
        }).catch(err => {
            callback(err);
        });
};

export const getAll = async callback => {
    await users.findAll({attributes : ['id' , 'name' , 'username' , 'email_id' , 'contact_number']})
        .then(UserData => {
            if(UserData == null){
                callback(null, null);
            }else{
                callback(null, UserData);
            }
        }).catch(err => {
            callback(err);
        });
};

export const user_delete = async (id , callback) => {
  
    await users.destroy({where: { id }})
        .then(num => {
          callback(null, num)
        })
        .catch(err => {
          callback(err)
        });
};

export const user_update = async (id ,userData, callback) => {
  
    await users.update(userData, { where: { id }})
        .then(num => {
          callback(null, num);
        })
        .catch(err => {
          callback(err);
        });
};
