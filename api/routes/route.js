import express from 'express';
const router = express.Router();

import { router as users } from './route/users.route.js';
import { router as login } from './route/login.route.js';

router.use('/users',users);
router.use('/login',login);

export {router};
