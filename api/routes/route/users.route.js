import express from 'express';
const router = express.Router();
import {validate} from '../../helper/validator.helper.js';
import {user , userUpdate} from '../../validation/user.validation.js';
import {registerUser , getAllUsers , removeUser , updateUser, getUserById} from '../../controller/users.controller.js';
import { authenticate } from '../../helper/auth.helper.js';

router.post('/register' , validate.body(user.schemaUser) , registerUser);
router.put('/update/:id' , authenticate, validate.body(userUpdate.schemaUser) , updateUser);
router.get('/',authenticate,getAllUsers);
router.get('/:id',authenticate, getUserById);
router.delete('/delete/:id', authenticate, removeUser);

export {router};

