import express from 'express';
const router = express.Router();
import {validate} from '../../helper/validator.helper.js';
import { login } from '../../validation/login.validation.js';
import { userLogin } from '../../controller/login.controller.js';

router.post('/' , validate.body(login.schemaLogin) , userLogin);

export {router};