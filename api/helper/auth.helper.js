import jwt from 'jsonwebtoken';
import {JWT} from '../database/database.config.js';
import error from '../utils/error.js';

export const authenticate = (req, res, next) => {
    const jwtPrivateKey = JWT.jwtPrivateKey;

    try{
        const token = req.headers['auth_token'];
        
        if(token !== undefined){
        
            jwt.verify(token, jwtPrivateKey, (err, decoded) => {
                if (err) {
                  next(new error.UnAuthorized('auth token is invalid'));
                } else {
                  req.decoded = decoded;
                  next();
                }
              });
        }else{
            next(new error.UnAuthorized('auth token not supplied'));
        }
    }catch(ex){
        next(new error.UnAuthorized('authorization error'));
    }
}
