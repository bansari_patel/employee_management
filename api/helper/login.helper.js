import jwt from 'jsonwebtoken';
import {JWT} from '../database/database.config.js'


export const generateAuthToken = (user) => {
    return jwt.sign({ id : user.id,email_id: user.email_id},JWT.jwtPrivateKey,{expiresIn: JWT.TOKEN_EXPIRY});
};
