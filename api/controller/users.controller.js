import { user_create , isUserExistById , getAll, user_delete , user_update} from '../services/user.service.js';
import bcrypt from 'bcrypt';
import { GeneralResponse ,CreatedResponse } from '../utils/response.js';
import error from '../utils/error.js';
import {config} from '../utils/config.js';
import _ from 'lodash'
const hashRound = 10;
const getError = 'Error while getting user';



export const registerUser = async (req, res, next) => {
    try {

        req.body.password = await bcrypt.hash(req.body.password, hashRound);
        const userData = req.body;

        await user_create(userData, (err, response) => {
            if (err) {
                if(err.fields.username){
                    next(new error.BadRequest('username already exist.Please Enter different username'));
                }else if(err.fields.email_id){
                    next(new error.BadRequest('email_id already exist.Please Enter different email_id'));
                }else{
                    next(new error.GeneralError('user registration failed'));
                }
            } else {
                next(new CreatedResponse('user successfully registered', undefined, config.HTTP_CREATED));
            }
        });
    } catch (err) {
        next(new error.GeneralError('user registration failed'));
    }
}

export const getUserById = async (req, res, next) => {
    try {
        const id = req.params.id;

        await isUserExistById(id, (err, response) => {
            if (typeof response != 'undefined' && response === null) {
                next(new error.NotFound('no user found'));
            } else if (err) {
                next(new error.GeneralError(getError));
            } else {
                next(new GeneralResponse('User details found',_.pick(response , [ 'id' , 'name' , 'email_id' , 'contact_number' , 'username']) ));
            }
        });

    } catch (err) {
        next(new error.GeneralError(getError));
    }
}

export const getAllUsers = async (req, res, next) => {
    try {
        await getAll((err, response) => {
            if(typeof response != 'undefined' && response == null){
                next(new error.NotFound('No user found'));
            }else if(err) {
                next(new error.GeneralError(getError));
            }else{
                next(new GeneralResponse('Users detail found', response));
            }
        });
    } catch (err) {
        next(new error.GeneralError(getError));
    }
}

export const removeUser = async (req, res, next) => {
    try {
        const id = req.params.id;
        await user_delete(id, (err, response) => {
            if(typeof response != 'undefined' && response === 0){
                next(new error.NotFound('No user found'));
            }else if(err) {
                next(new error.GeneralError(getError));
            }else{
                next(new GeneralResponse('User removed successfully', undefined, config.HTTP_SUCCESS));
            }
        });
    } catch (err) {
        next(new error.GeneralError(getError));
    }
}

export const updateUser = async (req, res, next) => {
    try {
        const id = req.params.id;
        if(req.body.password){
            req.body.password = await bcrypt.hash(req.body.password, hashRound);
        }

        await user_update( id , req.body, (err, response) => {
            if(typeof response != 'undefined' && response[0] === 0){
                next(new error.NotFound('No user found'));
            }else if(err) {
                if(err.fields.username){
                    next(new error.BadRequest('username already exist.Please Enter different username'));
                }else if(err.fields.email_id){
                    next(new error.BadRequest('email_id already exist.Please Enter different email_id'));
                }else{
                    next(new error.GeneralError('Error while updating user'));
                }
            }else{
                next(new GeneralResponse('User updated successfully', undefined, config.HTTP_SUCCESS));
            }
        });
    } catch (err) {
        next(new error.GeneralError('Error while updating user'));
    }
}
