
import { isUserExistByEmail, isUserExistByUsername } from '../services/user.service.js';
import { login_create } from '../services/login.service.js';
import { GeneralResponse } from '../utils/response.js';
import bcrypt from 'bcrypt';
import error from '../utils/error.js';
import { generateAuthToken } from '../helper/login.helper.js';
const hashRound = 10;
let flag = 0;

export const userLogin = async (req, res, next) => {
    try {
        const hashPassword = await bcrypt.hash(req.body.password, hashRound);
        await isUserExistByEmail(req.body.email_id_or_username, async (err, response) => {

            if (err || response == null) {
                await isUserExistByUsername(req.body.email_id_or_username, async (Err, Response) => {
                    if (Err || Response == null) {
                        next(new error.UnAuthorized('Invalid username or email'));
                    } else {
                        flag = 1;
                        response = Response;
                    }
                })
            } else {
                flag = 1;
            }

            if(flag == 1) {
                bcrypt.compare(req.body.password, response.password, (ERR, RES)=>{
                    if (ERR) {
                        next(new error.UnAuthorized('Invalid login details'));
                    } else if(RES) {
                        const token = generateAuthToken(response);
        
                        req.body.password = hashPassword;
        
                        login_create(req.body, (errAuth, responseAuth) => {
                            if (errAuth == null) {
                                const loginResponse = {
                                    auth_token: token
                                }
                                next(new GeneralResponse('Authorized user', loginResponse));
                            } else {
                                next(new error.GeneralError('User Login failed'));
                            }
                        })
           
                    }else{
                        next(new error.UnAuthorized('Invalid login details'));
                    }
                })
            }
        });
    } catch (err) {
        next(new error.GeneralError('User Login failed.'));
    }
}

