import config from 'nconf';
import Sequelize from 'sequelize';
import { loginModel } from '../models/login.model.js';
import { userModel } from '../models/users.model.js';
import winston from 'winston';

winston.add(new winston.transports.Console,{
    level: 'info', colorize: true });

const env = process.env.NODE_ENV || 'development';

winston.info(`In ${env} Environment`)

config.file(`${env}`,{ file: `api/config/${env}.json`});
export const dbconfig = config.get('dbConfig');
export const JWT = config.get('jwt');

const sequelize = new Sequelize(dbconfig.database, dbconfig.user, dbconfig.password, {
    host: dbconfig.host,
    dialect: 'mysql',
    timezone: '+09:00',
    define: {
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci',
        underscored: true,
        freezeTableName: true,
        timestamps: false,
        createdAt: false,
        updatedAt: false
    },
    pool: {
        min: dbconfig.pool.min,
        max: dbconfig.pool.max,
    },
    logging: false,
    benchmark: true,
});

async function checkConnection(){
    try {
        await sequelize.authenticate();
        winston.info('Connection has been established successfully.');
    }catch (error){
        winston.error('Unable to connect to the database:', error);
    }
    
    await sequelize.sync();
    winston.info("All models were synchronized successfully.");
}
checkConnection();


const db = {};


db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.user = userModel(sequelize, Sequelize)
db.login = loginModel(sequelize, Sequelize)

export default db;
