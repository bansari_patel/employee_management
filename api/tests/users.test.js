import chai from 'chai';
import chaiHttp from 'chai-http';
import { server } from '../../server.js';
import {generateAuthToken} from '../helper/login.helper.js';
import db from '../database/database.config.js';
import bcrypt from 'bcrypt'

const User = db.user;

chai.should();

chai.use(chaiHttp);


describe('Users API', function() {

    var token;
    var user;
    
    before(async () => {

        User.destroy({ truncate: true , cascade: false });

        await new Promise(resolve => setTimeout(resolve,5000));
        
        user = User.create({
            name: 'xyzwa',
            email_id: 'xyz@gmail.com',
            password: await bcrypt.hash('Xyz@1234', 10),
            contact_number: '1234567890',
            username : 'xyz1234'
        }).then(() => done());

        token = generateAuthToken(user);

    });


    describe('GET All', () => {
        it('It should GET all the users', (done) => {
            chai.request(server)
            .get('/api/route/users')
            .set('auth_token', token)
            .end((err , response) =>{
                response.should.have.status(200);
                response.body.should.be.a('object');
            done();
            });
            
        });
        
        it('It should not GET all the users', (done) => {
            chai.request(server)
                .get('/api/route/user')
                .set('auth_token', token)
                .end((err , response) =>{
                    response.should.have.status(404);
                done();
                });
        });
        
        it('It should not GET all the users without proper auth_token', (done) => {
            let new_token = ''
            chai.request(server)
                .get('/api/route/users')
                .set('auth_token', new_token)
                .end((err , response) =>{
                    response.should.have.status(401);
                done();
                });
        });
    });
    
    
    describe('GET /:id', () => {
        it('It should GET the user by id', (done) => {
            const id = 1
            chai.request(server)
                .get('/api/route/users/'+ id)
                .set('auth_token', token)
                .end((err , response) =>{
                    response.should.have.status(200);
                    response.body.should.have.property('result')
                        .which.is.an('object')
                        .and.have.property('id' && 'name' && 'email_id' && 'password' && 'username' && 'contact_number')
                done();
                });
            
            
        });
        
        it('It should not GET the user by invalid id', (done) => {
            const id = 0.1;
            chai.request(server)
                .get('/api/route/users/'+ id)
                .set('auth_token', token)
                .end((err , response) =>{
                    response.should.have.status(404);
                done();
                });
            
            
        });
        
        it('It should not GET the user user by id without proper auth_token', (done) => {
            const id = 1
            const new_token = '';
            chai.request(server)
                .get('/api/route/users/'+ id)
                .set('auth_token', new_token)
                .end((err , response) =>{
                    response.should.have.status(401);
                done();
                });
            
            
        });
    });
    
    
    describe('POST /', () => {
        it('It should POST a new user',(done) => {
            const userData = {
                'name': 'abcde',
                'email_id': 'x1y@gmail.com',
                'password': 'Xyz@1234',
                'contact_number': '1234567890',
                'username' : 'x1yz14'
            }
            chai.request(server)
                .post('/api/route/users/register')
                .send(userData)
                .end((err , response) =>{
                    response.should.have.status(201);
                    response.body.should.have.property('message')
                        .which.is.an('string')
                done();
                });
        });
        
        it('It should not POST a new user',(done) => {
            const userData = {
                'name': 'abcde',
                'email_id': 'x1y@gmail.com',
                'password': 'Xyz@1234',
                'contact_number': '1234567890',
                'username' : 'x1yz14'
            }
            chai.request(server)
                .post('/api/route/users/register')
                .send(userData)
                .end((err , response) =>{
                    response.should.have.status(400);
                    response.body.should.have.property('message')
                        .which.is.an('string');
                done();
                });
        });
    });


    describe('PUT /', () => {
        it('It should update an existing user',(done) => {
            const userData = {
                'name': 'abcdef',
                'email_id': 'xyz@gmail.com',
                'password': 'Xyz@1234',
                'contact_number': '1234567890',
                'username' : 'abc1234'
            }
            const id = 1;
            chai.request(server)
                .put('/api/route/users/update/' + id)
                .set('auth_token', token)
                .send(userData)
                .end((err , response) =>{
                    response.should.have.status(200);
                    response.body.should.have.property('message')
                        .which.is.an('string')
                done();
                });
        });

        it('It should not update an existing user with an invalid id',(done) => {
            const userData = {
                'name': 'abcdef',
                'email_id': 'xyz@gmail.com',
                'password': 'Xyz@1234',
                'contact_number': '1234567890',
                'username' : 'bansari'
            }
            const id = 0.1;
            chai.request(server)
                .put('/api/route/users/update/3')
                .set('auth_token', token)
                .send(userData)
                .end((err , response) =>{
                    response.should.have.status(404);
                done();
                });
        });
        
        it('It should not update an existing user',(done) => {
            const userData = {
                'name': 'abcdef',
                'email_id': 'x1y@gmail.com',
                'password': 'Xyz@1234',
                'contact_number': '1234567890',
                'username' : 'abc1234'
            }
            const id = 1;
            chai.request(server)
                .put('/api/route/users/update/' + id)
                .set('auth_token', token)
                .send(userData)
                .end((err , response) =>{
                    response.should.have.status(400);
                    response.body.should.have.property('message')
                        .which.is.an('string');
                done();
                });
        });

        it('It should not update an existing user without proper auth_token', (done) => {
            const id = 1
            const new_token = '';
            chai.request(server)
                .put('/api/route/users/update/'+ id)
                .set('auth_token', new_token)
                .end((err , response) =>{
                    response.should.have.status(401);
                done();
                });
            
            
        });
    });
    
    
    describe('DELETE /', () => {
        it('It should delete an existing user',(done) => {
            const id = 1;
            chai.request(server)
                .delete('/api/route/users/delete/' + id)
                .set('auth_token', token)
                .end((err , response) =>{
                    response.should.have.status(200);
                    response.body.should.have.property('message')
                        .which.is.an('string')
                done();
                });
        });

        it('It should not delete an existing user with an invalid id',(done) => {
            const id = 0.3;
            chai.request(server)
                .delete('/api/route/users/delete/'+ id)
                .set('auth_token', token)
                .end((err , response) =>{
                    response.should.have.status(404);
                done();
                });
        });
        
        it('It should not delete an existing user without proper auth_token', (done) => {
            const id = 2
            const new_token = '';
            chai.request(server)
                .delete('/api/route/users/delete/'+ id)
                .set('auth_token', new_token)
                .end((err , response) =>{
                    response.should.have.status(401);
                done();
                });
            
            
        });
    });
});